<?php
    include 'includes/site-metas.php';
?>
    <title>Framework</title>
</head>
<body class="inicioaberto">
    <?php
        include 'includes/site-cabecalho.php';
    ?>

    <main>
        <div class="container">
            <a class="botao1" href="introducao.php">Introdução</a>
        </div>
    </main>

    <?php
        include 'includes/site-rodape.php';
    ?>

    <link rel="stylesheet" href="assets/css/home.css">
    <script src="assets/js/home.js"></script>
</body>
</html>
