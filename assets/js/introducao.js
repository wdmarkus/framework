$(document).ready(function(){
    $('.introslider .slider').flexslider({
        selector: ".lista-slider > .item-lista-slider",
        animation: "slide",
        slideshow: false,
        animationLoop: false,
        move: 1,
        // directionNav: false,
        // controlNav: false,
        customDirectionNav: $(".introslider .controlesslider > a"),
        controlsContainer: $(".introslider .paginacao-controlesslider")
    });
});
