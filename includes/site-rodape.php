<footer class="rodape">
    <div class="container">
    </div>
</footer>

<div id="modal">
    <span class="fechar-modal"><?= file_get_contents('assets/img/icones/fechar-circulo.svg') ?></span>

    <div class="container">
        <div class="wrap-mensagemmodal wrap-modal">
            <div class="texto-modal">
                <div class="icone-texto-modal"></div><p></p>
            </div>
            <div class="botao-mensagemmodal">
                <a class="botao2" href=""></a>
            </div>
        </div>

        <div class="wrap-imagemmodal wrap-modal">
            <div class="imagem-imagemmodal">
                <img src="" alt="">
            </div>
            <div class="legenda-imagemmodal"></div>
        </div>

        <div class="wrap-carregarmodal wrap-modal">
            <span class="icone-carregarmodal"></span>
        </div>

        <div class="wrap-videomodal wrap-modal">
            <div class="videoresponsivo">
            </div>
        </div>

        <div class="wrap-menumodal wrap-modal">
            <nav class="menu-menumodal">
                <?php
                    include 'site-menu.php';
                ?>
            </nav>
        </div>
    </div>
</div>

<div class="mascara"></div>

<script src="assets/js/libs/jquery.js"></script>
<script src="assets/js/libs/mask.js"></script>
<script src="assets/js/libs/SmoothScroll.js"></script>
<script src="assets/js/libs/flexslider.js"></script>
<script src="assets/js/framework.js"></script>
<script src="assets/js/geral.js"></script>
