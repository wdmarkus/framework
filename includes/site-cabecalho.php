<div class="inicio">
    <div class="wrap-inicio">
    </div>
</div>
<div class="logo-inicio">
    <?= file_get_contents('assets/img/logo.svg') ?>
</div>
<span class="icone-inicio"></span>

<header class="cabecalho">
    <div class="container">
        <a href="#" class="logo-cabecalho">
            <?= file_get_contents('assets/img/logo.svg') ?>
        </a>
        <nav class="menu-cabecalho">
            <?php
                include 'site-menu.php';
            ?>
        </nav>
        <!-- <button id="hamburguer">
            <div>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button> -->
    </div>
</header>

<div class="banner">
    <div class="table">
        <div class="cell-table">
            <p>Banner</p>
        </div>
    </div>
</div>
